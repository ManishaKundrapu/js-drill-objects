const elements = require('./data.cjs');

function pairs(elements) {

    const result = [];

    if (typeof (elements) !== 'object' || pairs.arguments.length !== 1) {

        return [];

    }

    for (let key in elements) {

        result.push([key, elements[key]]);
    }

    return result;

}

module.exports = pairs;