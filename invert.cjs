const elements = require('./data.cjs');

function invert(elements) {
    const result = {};

    if (typeof (elements) !== 'object' || invert.arguments.length !== 1) {
        return {};
    }

    for (let key in elements) {
        result[elements[key]] = key;

    }
    return result;
}

module.exports = invert;
