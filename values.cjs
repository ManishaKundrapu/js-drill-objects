const elements = require('./data.cjs');

function values(elements) {
    const result = [];

    if (typeof(elements) !== 'object' || values.arguments.length !== 1) {
        return [];
    }
    
    for (let value in elements) {
        result.push(elements[value]);
    }
    return result;
}

module.exports = values;
