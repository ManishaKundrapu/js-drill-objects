const data = require('../data.cjs');
const values = require('../values.cjs');

test('Testing values function', () => {
    expect(values(data)).toStrictEqual(Object.values(data));
});
