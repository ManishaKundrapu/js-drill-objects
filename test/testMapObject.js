const data = require("../data.cjs");
const mapObject = require("../mapObject.cjs");

function cb(value) {
  if (typeof value === "string") {
    return value + "10";
  } else {
    return value + 2;
  }
}

const result = {name: "Bruce Wayne10", age: 38, location: "Gotham10"};

test("Testing mapObject function", () => {
  expect(mapObject(data, cb)).toStrictEqual(result);
});
