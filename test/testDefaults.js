const elements = require("../data.cjs");
const defaults = require("../defaults.cjs");

const defaultProps = {flavor: "chocolate", sprinkles: "less"};

const result = {
  name: "Bruce Wayne",
  age: 36,
  location: "Gotham",
  flavor: "chocolate",
  sprinkles: "less",
};

test('Testing Defaults function', () => {
    expect(defaults(elements, defaultProps)).toStrictEqual(result);
});

