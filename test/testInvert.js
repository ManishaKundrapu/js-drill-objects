const data = require('../data.cjs');
const invert = require('../invert.cjs');

const flipped = Object.entries(data).map(([key, value]) => [value, key]);

test('Testing Invert Function', () => {
    expect(invert(data)).toStrictEqual(Object.fromEntries(flipped));
});
