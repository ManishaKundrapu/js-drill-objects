const elements = require('./data.cjs');

function mapObject(elements, cb) {
    const resultObj = {};

    if (typeof (elements) !== 'object' || mapObject.arguments.length < 2) {
        return {};
    }

    for (let key in elements) {

        resultObj[key] = cb(elements[key]);
        
    }
    return resultObj;
}

module.exports = mapObject;
